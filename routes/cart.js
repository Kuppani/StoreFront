const cartRouter = require('express').Router();
const Cart = require('../methods/cart');

const Product = require('../models/product');
const Plan = require('../models/plan');

const getFromDB = (req, res, cartCallback) => {
	let cart = new Cart(req.session.cart || {});
	const collectionId = req.body.collectionId || req.body.collectionID;
	console.log('\nadd/remove cart bodyParams\n', req.body);
	const sid = req.sessionID;
	console.log("session id: "+sid);
	cartCallback(cart, collectionId);
}

cartRouter.post('/addproduct', (req, res) => {
	const addProduct = (cart, collectionId) => {

		const addCallback = (err, productData) => {
			console.log('The product data\n\n', productData);
			cart.addProduct(collectionId, productData);
			req.session.cart = cart; //saves the cart to session
			console.log("product cart data: "+cart);
			res.send(cart);
		};
		Product.selectTable('pravallika_store_data');
		Product.getItem({
			collectionType: 'product',
			collectionId: collectionId
		}, null, addCallback);
	}
	getFromDB(req, res, addProduct);
});

cartRouter.post('/addplan', (req, res) => {
	const addPlan = (cart, collectionId) => {
		const getCallback = (err, planData) => {
			console.log('The plan data\n\n', planData);
			// cart.frequency = planData.frequency;
			// cart.unit = planData.unit;
			// cart.requestType = planData.requestType; //setting the payment mode as subscription
			// cart.add(planData, collectionId, 'plan', () => {console.log('added Plan');});
			cart.addPlan(collectionId, planData);
			req.session.cart = cart; //saves the cart to session
			console.log("plan cart data: "+cart);
			res.send(cart);
		};
		Plan.selectTable('pravallika_store_data');
		Plan.getItem({
			collectionType: 'plan',
			collectionId: collectionId
		}, null, getCallback);
	}
	getFromDB(req, res, addPlan);
});

cartRouter.post('/removeitem', (req, res) => {
	const collectionType=req.body.collectionType;
	console.log("Type:"+collectionType); 
	getFromDB(req, res, (cart, collectionId) => {
		if (collectionType.toLowerCase() === 'product') {
			cart.removeProduct(collectionId);
		} else {
			cart.removePlan(collectionId);
		}
		req.session.cart = cart;
		res.send(cart);
	});
});

cartRouter.post('/additem', (req, res) => {
	const collectionType=req.body.collectionType;
	console.log("Type:"+collectionType); 
	getFromDB(req, res, (cart, collectionId) => {
		if (collectionType.toLowerCase() === 'product') {
			cart.ProductQuant(collectionId);
		} else {
			cart.PlanQuant(collectionId);
		}
		req.session.cart = cart;
		res.send(cart);
	});
});

cartRouter.post('/removecart', (req, res) => {
	const collectionType=req.body.collectionType;
	console.log("Type:"+collectionType); 
	getFromDB(req, res, (cart, collectionId) => {
		if (collectionType.toLowerCase() === 'product') {
			cart.removeProductCart(collectionId);
		} else {
			cart.removePlanCart(collectionId);
		}
		req.session.cart = cart;
		res.send(cart);
	});
});

module.exports = cartRouter;