const router = require('express').Router();

const Product = require('../models/product');
const Plan = require('../models/plan');
const Storename = require('../models/storename');
const middleware = require('../middleware/index');

//router middleware for this file
router.use((req, res, next) => {
	res.locals.formUrl = '/checkout';
	next();
})

router.get('/', (req, res) => {
	
	console.log(req.get('host'));
	const url=req.get('host');
	const store=url.split(".");
	console.log(store[0]);
	const storeName=store[0];
	Storename.selectTable('store_details');
	Storename.getItem(storeName, {}, (err, name) => {
		console.log(name, err);
		if (err) {
			return res.send(statusMsg.errorResponse(err))
		} if (Object.keys(name).length === 0) {
			console.log("storename not found");
			res.send("storename not found");
		}
		if (Object.keys(name).length > 0) {
			// add a fallback in case there is an error fetching the product/plan data from the database
				const table=storeName+"_store_data";
				Product.selectTable(table);
				Product.query('product', (err, productItems) => {
					const json=JSON.stringify(productItems);
					res.locals.products = productItems || {};
					res.locals.productCount = productItems.length || 0;
					
					res.render('index');
				});
		}
	});

});

const publicPages = [
	'about',
	'ingredients',
	'privacy',
	'refund',
	'shipping',
	'terms',
];

router.get('/:page', (req, res, next) => {
	const tempPage = req.params.page;
	if (publicPages.indexOf(tempPage) > -1) {
		res.render(`${tempPage}`)
	} else {
		next();
	}
});

router.get('/products', (req, res) => {
	const Url = req.get('host');
	const store=Url.split(".");
	const table=store[0]+"_store_data";
	Product.selectTable(table);
	Product.query('product', (err, productItems) => {
		if (err) {
			console.log('\nerr:\n', err);
			//Fallback incase no products are fetched
			res.locals.products = {};
			res.locals.productCount = 0;
			req.flash('error', 'Failed to fetch products!');
			return res.redirect('/products');
		}
		res.locals.products = productItems || {};
		res.locals.productCount = productItems.length || 0;
		res.render('products');
	});
});

router.get('/profile', middleware.isLoggedIn, (req, res, next) => {
	res.render('profile');
});

module.exports = router;