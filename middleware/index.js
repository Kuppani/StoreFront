module.exports.isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    } else {
        console.log('\nError: not logged in!\n');
        req.flash("error", "You need to be logged in!");
        req.session.save(()=>{
            return res.redirect('/');
        });
    }
}