$(function () {

    var cartCount = $('.cart-total-items').val();

    localStorage.cart = {};

    var getBaseUrl = function () {
        var re = new RegExp(/^.*\//);
        return re.exec(window.location.href);
    };
    
    var getRootUrl = function () {
        return window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
    };
    
    var rootUrl = getRootUrl();
    var params = {};

    var modifyCart = function (data, itemId, addition) {
        //If the item we are trying to add already exists, then only increase it's count in the DOM instead of appending it to the cart
        if ($(itemId).length > 0) {
            var quantity = $(itemId).children('.cart-item-quantity');
            var value = parseInt($(quantity).children('.cart-item-qty').text());
    
            if (addition == true) {
                cartCount++;
                $('#notifyEmptyCart').hide();
                $(quantity).children('.cart-item-qty').text(value + 1);
            } 
            else if(addition == 'remove'){
                cartCount=cartCount-value;
                $(itemId).remove();

            }
            else {
                if (cartCount > 0) {
                    //Reduce the total cartcount only if it's positive
                    cartCount--;
                }
                if (value > 1) {
                    //The item quantity should be reduced if it's present
                    $(quantity).children('.cart-item-qty').text(value - 1);
                }
                //If threre's only one pack of product, on clicking remove button it should be reomved from the cart
                else if (value <= 1) {
                    $(itemId).remove();
                }
            }
        }
        //Else if there's no item already in the cart then add it 
        else if (data) {
            cartCount++;
            itemId = itemId.replace("#", "");
            var template = `<div class="cart-item" id="${itemId}">
            <span class="cart-item-image">
                <img src="${data.item.imageURL}" alt="product-image">
            </span>
            <span class="cart-item-name">
            ${data.item.title}
            </span>
            <span class="cart-item-price">
                ₹ ${data.item.price}
            </span>
            <span class="cart-item-sub sub-product" data-id="${data.item.collectionType}">
                <span class="fa fa-minus"></span>
            </span>
            <span class="cart-item-quantity">
                <span class="cart-item-qty">1</span>
            </span>
            <span class="cart-item-add add-product" data-id="${data.item.collectionType}">
                <span class="fa fa-plus"></span>
            </span>
            <span class="cart-item-remove remove-product" data-id="${data.item.collectionType}">
                <span class="fa fa-window-close"></span>
            </span>
        </div> `;
    
            $('#items').append(template);
        }
        // else {
        //     data.dataPresent = false;
        // }
        if (cartCount < 1) {
            //if there are no items in the cart then append a message to show that there are no items in the cart
            // $('#items').append('<p id="notifyEmptyCart">No items in cart</p>')
            $('#notifyEmptyCart').show();
        } else {
            //else remove the notification from the cart
            $('#notifyEmptyCart').hide();
        }
        console.log('\ncartCount\n', cartCount);
    };
    
    var logger = function (data, buttonId, addition) { //The value of addition is a boolean
        // console.log(data.totalQty, data.totalPrice);
        var itemId = '#item-' + buttonId;
        var dataToAdd = {};
        if ($(itemId).length == 0 && data.items[buttonId]) {
            dataToAdd = data.items[buttonId];
            // dataToAdd.dataPresent = true;
        }
        modifyCart(dataToAdd, itemId, addition);
    
        $('.cart-total-items').text(data.totalQty);
        $('#total-total').text(data.totalPrice);
        localStorage.cart = JSON.stringify(data);
        console.log(`\ndone..\n`, data);
    };

    var addCallback = function(e) {
        var buttonId = $(this).attr('id');
        var type=$('#'+buttonId).attr('data-id');
        console.log("Type :"+type);
        //console.log('buttonId: ', buttonId);
        var url = rootUrl;
        params.collectionId = buttonId;
        // cartCount++;
        /*if (buttonId.toLowerCase().indexOf("plan") >= 0) {
            url += 'addplan';
        } else {
            url += 'addproduct';
        }*/
        if(type == 'plan'){
            url += 'addplan';
        }
        if(type == 'product'){
            url += 'addproduct';
        }
        console.log("add plan url: "+url);

        ajaxPost(url, params, function (data) {
            console.log('Adding item to the cart...');
            swal({
                type: 'success',
                title: 'Added to Cart!',
                showConfirmButton: false,
                timer: 1200
            })
            logger(data, buttonId, true);
        })
    };

    var removeCallback = function (e) {
        var buttonId = $(this).attr('id');
        buttonId = buttonId.replace("remove-", "");
        params.collectionId = buttonId;
        // if (cartCount > 0) {
        //     cartCount--;
        // }
        var url = rootUrl + 'removeitem';
        ajaxPost(url, params, function (data) {
            console.log('Deleting the item...');
            swal({
                type: 'error',
                title: 'Removed item!',
                showConfirmButton: false,
                timer: 1200
            });
            logger(data, buttonId, false);
        })

    };

    var deleteItemCallback = function () {
        var buttonId = $(this).parent().attr('id');
        buttonId = buttonId.replace('item-', '');
        var collectionType=$(this).attr('data-id');
        //console.log("type:"+type);

        params.collectionId = buttonId;
        params.collectionType=collectionType;
        // if (cartCount > 0) {
        //     cartCount--;
        // }
        var url = rootUrl + 'removeitem';
        ajaxPost(url, params, function (data) {
            console.log('Deleting the item...');
            swal({
                type: 'error',
                title: 'Removed item!',
                showConfirmButton: false,
                timer: 1200
            })
            logger(data, buttonId, false);
        });
    };

    var addItemCallback = function () {
        var buttonId = $(this).parent().attr('id');
        buttonId = buttonId.replace('item-', '');
        var collectionType=$(this).attr('data-id');
        //console.log("type:"+type);

        params.collectionId = buttonId;
        params.collectionType=collectionType;
        // if (cartCount > 0) {
        //     cartCount--;
        // }
        var url = rootUrl + 'additem';
        ajaxPost(url, params, function (data) {
            console.log('Updated quantity...');
            swal({
                type: 'success',
                title: 'Updated quantity!',
                showConfirmButton: false,
                timer: 1200
            })
            logger(data, buttonId, true);
        });
    };

    var removeCartCallback = function () {
        var buttonId = $(this).parent().attr('id');
        buttonId = buttonId.replace('item-', '');
        var collectionType=$(this).attr('data-id');
        //console.log("type:"+type);

        params.collectionId = buttonId;
        params.collectionType=collectionType;
        // if (cartCount > 0) {
        //     cartCount--;
        // }
        var url = rootUrl + 'removecart';
        ajaxPost(url, params, function (data) {
            console.log('Removed item...');
            swal({
                type: 'error',
                title: 'Removed item!',
                showConfirmButton: false,
                timer: 1200
            })
            logger(data, buttonId, 'remove');
        });
    };

    $('#products, #subscriptionplans').on('click', 'button.cstm-add-item', addCallback);
    $('#products,#subscriptionplans').on('click', 'button.remove-item', removeCallback);
    $('#items').on('click', '.cart-item-remove', removeCartCallback);
    $('#items').on('click', '.cart-item-sub', deleteItemCallback);
    $('#items').on('click', '.cart-item-add', addItemCallback);

});