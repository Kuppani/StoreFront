function ajaxcaller(tmpurl, method, params, callback) {
    return $.ajax({
        url: tmpurl,
        type: method,
        crossDomain: true,
        data:params,
        dataType: 'json',
        success: function (data) {
            console.log('Sucess!');
            callback(data);
        },
        error: function (err) {
            alert('Failed!');
            callback(err)
        }
    });
}

function ajaxGet(tmpurl, params, cb) {
    return ajaxcaller(tmpurl, 'get', params, cb);
}

function ajaxPost(tmpurl, params, cb) {
    return ajaxcaller(tmpurl, 'post', params, cb);
}
