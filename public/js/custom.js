$(function () {
    $('.cart-summary,.cart-link').click(function () {
        $('body').toggleClass('cart-widget-open');
    });

    $('#cart-widget-close,.cart-widget-close-overlay').click(function () {
        $('body').removeClass('cart-widget-open');
    });

    $('.switch-tab a').click(function () {
        var tempId = $(this).attr('href');
        $('#cart-tabs').find('a[href="' + tempId + '"]').click();
    });
    $('nav').on('click', '.dropdown-toggle', function () {
        $('.dropdrown-menu').toggle();
    });

    $('footer').on('click', '#scroll-top', function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });

    //Scroll to top
    $(window).scroll(function () {
        if ($('body').scrollTop() > 500 || $('html').scrollTop() > 500) {
            $('#scroll-top').show("fast");
        } else {
            $('#scroll-top').hide("fast");
        }
    });

    //Only allow numbers in input field
    $('form').on('keydown', '.cstm-num-pattern', function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    });
    //Enable dropdown toggle after the DOM has loaded
    $(".dropdown-toggle").dropdown();

    //Initialize swiper
    function swiperInitializer() {
        var swiper = new Swiper(".home-slider", {
                pagination: ".home-pagination",
                paginationClickable: !0,
                nextButton: ".home-slider-next",
                prevButton: ".home-slider-prev"
            }),
            swiper = new Swiper(".testimonials-slider", {
                pagination: ".testimonials-pagination",
                paginationClickable: !0,
                slidesPerView: 1,
                spaceBetween: 30,
                nextButton: ".testimonials-slider-next",
                prevButton: ".testimonials-slider-prev"
            }),
            swiper = new Swiper(".product-list-slider", {
                slidesPerView: 3,
                pagination: ".product-list-pagination",
                paginationClickable: !0,
                nextButton: ".product-list-slider-next",
                prevButton: ".product-list-slider-prev",
                spaceBetween: 30,
                breakpoints: {
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    },
                    420: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    }
                }
            }),
            //for swiper with subscription plans
            swiper = new Swiper(".post-slider", {
                pagination: ".post-pagination",
                paginationClickable: !0,
                nextButton: ".post-slider-next",
                prevButton: ".post-slider-prev",
                slidesPerView: 3,
                spaceBetween: 30,
                breakpoints: {
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    },
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 0
                    },
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 0
                    }
                }
            });
    }
    //WIll be pushed to event loop and executed after everything before has been completed
    setTimeout(() => {
        swiperInitializer();
    }, 0);
    
    $(".navbar-nav li a.smooth-scroll[href^='#'], a[href='#product'").on("click", function (e) {
        e.preventDefault();
        var t = this.hash;
        $("html, body").animate({
            scrollTop: $(this.hash).offset().top
        }, 300, function () {
            window.location.hash = t
        })
    });
});