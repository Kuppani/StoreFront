//chatbot functionality
var accessToken = "0cfda717bf1c43e5ba55f1df7b781844";
var baseUrl = "https://api.api.ai/v1/";

$(function () {

    function coreChat() {
        var text = $("#chat-input").val();
        generate_message(text, 'user');
        chatbotReply(text);
    }

    function chatbotReply(text) {
        var theMessage = text;
        $.ajax({
            type: "POST",
            url: baseUrl + "query",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            data: JSON.stringify({ query: theMessage, lang: "en", sessionId: "felixtest" }),
            success: function (data) {
                var textResponse = data.result.speech;
                generate_message(textResponse, 'bot')
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    //Design elements
    var INDEX = 0; //to track the chat number

    function generate_message(msg, type) {
        INDEX++;
        var str = '';
        // var avatar = './img/';
        // avatar += (type === 'user') ? 'social.png' : 'robot.png'
        str += "<div id='cm-msg-" + INDEX + "' class=\"chat-msg " + type + "\">";
        // str += "<span class=\"msg-avatar\">";
        // str += "<img src='" + avatar + "'>";
        // str += "<\/span>";
        str += "<div class=\"cm-msg-text\">";
        str += msg;
        str += "<\/div>";
        str += "<\/div>";
        $(".chat-logs").append(str);
        $("#cm-msg-" + INDEX).hide().fadeIn(300);

        if (type == 'user') {
            $("#chat-input").val('');
        }
        $(".chat-logs").stop().animate({ scrollTop: $(".chat-logs")[0].scrollHeight }, 1000);

    }

    //Event listeners
    $('#chatbotBody').on(".chat-btn", "click", function () {
        var value = $(this).attr("chat-value");
        var name = $(this).html();
        $("#chat-input").attr("disabled", false);
        generate_message(name, 'user');
    })

    $("#chat-submit").on('click', function (e) {
        coreChat();
    })

    $("#chat-circle").click(function () {
        $("#chat-circle").toggle('scale');
        $(".chat-box").toggle('scale');
    })

    $("#chat-input").keypress(function (e) {
        if (e.which == 13) {
            coreChat();
        }
    })

    $(".chat-box-toggle").click(function () {
        $("#chat-circle").toggle('scale');
        $(".chat-box").toggle('scale');
    })

})