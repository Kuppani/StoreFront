const Methods = require('./custom');
class Cart {
	constructor(oldCart) {
		this.items = oldCart.items || {};
		this.totalQty = oldCart.totalQty || 0;
		this.totalPlans = oldCart.totalPlans || 0;
		this.totalPrice = oldCart.totalPrice || 0;
		this.totalWeight = oldCart.totalWeight || 0;
		//Subscription parameters
		this.requestType = `${oldCart.requestType || 'DEFAULT'}`.toUpperCase();
	};

	//Cart class methods

	// These are the psuedo private methods
	_addItem_(itemType, id, item, callback) {
		if (id.length > 0) {
			const addToCart = (storedItem) => {
				// // Note: Since this is an arrow function, there is no 'this' variable attached to this. Instead, 'this' will be inherited from it's parent obj scope; Here it's scope is that of the _addItem_ method, which is the 'Cart' constructor object.
				this.totalQty++;
				this.totalPrice += storedItem.item.price;
				this.requestType = 'SUBSCRIBE';
				if (itemType.toLowerCase().indexOf('plan') > -1) {
					this.totalPlans++;
					this.requestType = 'SUBSCRIBE';
					//setting default subscription to one month
					this.frequency = this.frequency || 1;
					this.unit = this.unit || 'DAY';
					this.expiryDate = Methods.formatDate(new Date().setDate(new Date().getDate() + 7));
				}
			}
			let storedItem = this.items[id];
			if (!storedItem || storedItem === undefined) { //if the item doesn't exist initialize it
				storedItem = this.items[id] = {
					item: item,
					quantity: 1 // since we added the item now, the initial count will be one
				};
				addToCart(storedItem);
			} else {
				storedItem.quantity++; //increment the quantity of stored item if it already exists
				addToCart(storedItem);
			}
			callback = callback || console.log(`Added ${itemType || 'item'}!`);
		}
	}

	_removeItem_(itemType, id, callback) {
		let storedItem = this.items[id] || {};
		const storedItemLength = Object.keys(storedItem).length;
		const storedQty = (storedItemLength) ? storedItem.quantity : 0;
		if (storedItemLength > 0 && storedQty > 0 && this.totalQty > 0) {
			storedItem.quantity--;
			this.totalQty--;
			this.totalPrice -= storedItem.item.price;
			if(storedItem.quantity < 1) {
				this.items[id] = undefined;
			}
			if(itemType.toLowerCase().indexOf('plan')>-1) {
				this.totalPlans--;
				//Removing subscription parameters from the cart if it doesn't contain any subscription plans in it.
				if (this.totalPlans < 1) {
					this.requestType = 'DEFAULT';
					//setting properties of an object to undefined is a way of deleting them
					this.frequency = undefined;
					this.unit = undefined;
					// this.totalQty = this.totalPrice = this.totalWeight = this.totalPlans = 0;
				}
			}
		};
	}

	_AddQuant_(itemType, id, callback) {
		let storedItem = this.items[id] || {};
		const storedItemLength = Object.keys(storedItem).length;
		const storedQty = (storedItemLength) ? storedItem.quantity : 0;

		if (itemType.toLowerCase().indexOf('plan') > -1) {
			this.totalPlans++;
			this.requestType = 'SUBSCRIBE';
			//setting default subscription to one month
			this.frequency = this.frequency || 1;
			this.unit = this.unit || 'DAY';
			this.expiryDate = Methods.formatDate(new Date().setDate(new Date().getDate() + 7));
		}
		if (storedItemLength > 0 && storedQty > 0 && this.totalQty > 0) {
			storedItem.quantity++;
			this.totalQty++;
			this.totalPrice += storedItem.item.price;
		};
	}

	_removeCart_(itemType, id, callback) {
		let storedItem = this.items[id] || {};
		const storedItemLength = Object.keys(storedItem).length;
		const storedQty = (storedItemLength) ? storedItem.quantity : 0;
		if (storedItemLength > 0 && storedQty > 0 && this.totalQty > 0) {
			//storedItem.quantity--;
			this.totalQty -=storedItem.quantity;
			this.totalPrice -= storedItem.item.price * storedItem.quantity;
				this.items[id] = undefined;
			if(itemType.toLowerCase().indexOf('plan')>-1) {
				this.totalPlans-=storedItem.quantity;
			}
		};
	}

	//Newly added methods
	addProduct(id, item, callback) {
		this._addItem_ ('product', id, item, callback);
	};

	addPlan(id, item, callback) {
		this._addItem_('plan', id, item, callback);
	};

	removeProduct(id, callback) {
		this._removeItem_('product', id, callback);
	};

	removePlan(id, callback) {
		this._removeItem_('plan', id, callback);
	};

	removeProductCart(id, callback) {
		this._removeCart_('product', id, callback);
	};

	removePlanCart(id, callback) {
		this._removeCart_('plan', id, callback);
	};

	ProductQuant(id, callback) {
		this._AddQuant_('product', id, callback);
	};

	PlanQuant(id, callback) {
		this._AddQuant_('plan', id, callback);
	};
};

module.exports = Cart;