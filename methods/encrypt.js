const statusMsg = require('./statusMsg');
const bcrypt = require('bcryptjs');

const generateSalt = (res,parameter, userCallback) => {
	bcrypt.genSalt(10, (err, salt) => {
		if (err) {
			console.log(`\nmsg-1\n ${err.message}`);
			res.locals.error = err.message;
			return res.redirect('/');
		} else {
			bcrypt.hash(parameter, salt, (err, hashparameter) => {
				if (err) {
					console.log(`\nmsg-2\n ${err.message}`);
					res.locals.error = err.message;
					return res.redirect('/');
				} else {
					userCallback(hashparameter);
				}
			})
		}
	})
}

module.exports = {
	generateSalt,
}