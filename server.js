const app = require('./app');
const port = 8000;
app.listen(port);
console.log(`The server has started on port ${port}`);
