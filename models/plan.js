const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalKeys = ['imageURL', 'size','frequency','frequencyUnit'];

const planSchema = {
    hashKey: 'collectionType',
    rangeKey: 'collectionId',
    timestamps: true,
    schema: Joi.object({
        collectionType: Joi.string().valid(['plan','Plan']), //Plan
        collectionId: Joi.string().alphanum(),
        title: Joi.string(),
        description: Joi.string(),
        price: Joi.number().positive().min(1),
        imageURL: Joi.string(),
        size: Joi.string(),
        frequency: Joi.number().min(1),
        frequencyUnit:Joi.string(),
    }).optionalKeys(optionalKeys).unknown(true)
};

const attToGet = ['title','description','price','imageURL', 'size','frequency','frequencyUnit'];
const attToQuery = ['description','imageURL','price','size','title','collectionId'];

const optionsObj  = {
    attToGet,
    attToQuery,
    //tableName:'pravallika_store_data'
};

const Plan = SchemaModel(planSchema,optionsObj);

module.exports = Plan;